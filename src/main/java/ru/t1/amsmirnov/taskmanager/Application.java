package ru.t1.amsmirnov.taskmanager;

import ru.t1.amsmirnov.taskmanager.constant.CommandConst;
import ru.t1.amsmirnov.taskmanager.constant.ArgumentConst;

import java.util.Scanner;


public final class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
        System.exit(0);
    }

    public static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            default:
                showArgumentError();
        }
    }

    public static void processCommand(final String command) {
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showArgumentError() {
        System.out.println("[ERROR]");
        System.out.println("This argument is not supported!");
        System.exit(1);
    }

    public static void showCommandError() {
        System.out.println("[ERROR]");
        System.out.println("This command is not supported!");
        System.exit(1);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Smirnov Anton");
        System.out.println("email: amsmirnov@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s : Show developer info. \n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s : Show version info. \n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s : Show command list. \n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s : Close Task Manager. \n", CommandConst.EXIT);
    }

}
